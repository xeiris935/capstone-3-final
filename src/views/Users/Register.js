import React, { useState } from 'react';
import {FormInput} from '../../globalcomponents';
import {
    Button
} from 'reactstrap';
import axios from 'axios';
import {NavigationBar, Footer} from '../Layout'

const Register = () => {

    const[firstName,setFirstName]= useState("");
    const[lastName,setLastName]= useState("");
    const[email,setEmail]= useState("");
    const[password,setPassword]=useState("");

    const handleFirstNameChange = (e) => {
        setFirstName(e.target.value)
        console.log(e.target.value)
    }

    const handleLastNameChange = (e) => {
        setLastName(e.target.value)
        console.log(e.target.value)
    }

    const handleEmailChange = (e) => {
        setEmail(e.target.value)
        console.log(e.target.value)
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value)
        console.log(e.target.value)
    }

    const handleRegister = () => {
        axios.post('https://warm-everglades-62190.herokuapp.com/admin/register',{
            firstName:firstName,
            lastName:lastName,
            email:email,
            password:password
        }).then(res=>console.log(res.data))

        window.location.replace('#/login');
    }

    const handleRedirectLogin = () => {
        window.location.replace('#/login');
    }

    return (
        <React.Fragment>
            <NavigationBar />
            <div className="register-container">
                <h1 className="header-text">Register</h1>
                <div className="form-container col-lg-4 offset-lg-4">
                    <FormInput
                        label={"First Name"}
                        placeholder={"Enter your first name"}
                        type={"text"}
                        onChange={handleFirstNameChange}
                    />
                    <FormInput
                        label={"Last Name"}
                        placeholder={"Enter your last name"}
                        type={"text"}
                        onChange={handleLastNameChange}
                    />
                    <FormInput
                        label={"Email"}
                        placeholder={"Enter Your Email"}
                        type={"email"}
                        onChange={handleEmailChange}
                    />
                    <FormInput
                        label={"Password"}
                        placeholder={"Enter Your Password"}
                        type={"password"}
                        onChange={handlePasswordChange}
                    />
                    <Button
                        block
                        // color="success"
                        className="success"
                        onClick={handleRegister}
                    >
                        Register
                    </Button>
                    <p className="card-text">Already have an account? 
                        <button 
                            className="register-button"
                            onClick={handleRedirectLogin}
                        >Click here!</button>
                    </p>
                </div>
            </div>
            <Footer />
        </React.Fragment>
    );
}
export default Register;
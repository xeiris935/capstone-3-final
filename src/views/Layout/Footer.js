import React from 'react';

const Footer = () => {
	return(
		<React.Fragment>
			<div className="footer">
				<p className="footer-text">All assets are not owned by me and is used for education purposes only. Created by: Arvin Lacdao - Batch 46</p>
			</div>
		</React.Fragment>
	)
}

export default Footer;
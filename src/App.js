import React from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';

const Register = React.lazy(()=>import('./views/Users/Register'));
const Login = React.lazy(()=>import('./views/Users/Login'));
const Courses = React.lazy(()=>import('./views/Courses/Courses'));
const Classes = React.lazy(()=>import('./views/Classes/Classes'));
// const Home = React.lazy(()=>import('./views/Layout/Home'));

const loading = () => {
    return(
    <div className="loading-screen">
      <div className="glitch-container">
        <h2 id="transmission-glitch1">Incoming transmission... Please wait</h2>
        <h2 id="transmission-glitch2">Incoming transmission... Please wait</h2>
        <h2 id="transmission-glitch3">Incoming transmission... Please wait</h2> 
      </div>
      <div className="glitch-container">
        <h1 id="loading-glitch1">Loading</h1>
        <h1 id="loading-glitch2">Loading</h1>
        <h1 id="loading-glitch3">Loading</h1>  
      </div>
    </div>
    )
}

const App = () =>{
  return (
      <HashRouter>
          <React.Suspense fallback={loading()}>
              <Switch>
                  <Route
                      path="/"
                      exact
                      name="Login"
                      render={props => <Login {...props} />}
                  >
                  </Route>
                  <Route
                      path="/courses"
                      exact
                      name="Courses"
                      render={props => <Courses {...props} />}
                  >
                  </Route>
                  <Route
                      path="/classes"
                      exact
                      name="Classes"
                      render={props => <Classes {...props} />}
                  >
                  </Route>
                  <Route
                      path="/register"
                      exact
                      name="Register"
                      render={props => <Register {...props} />}
                  >
                  </Route>
                  <Route
                      path="/login"
                      exact
                      name="Login"
                      render={props => <Login {...props} />}
                  >
                  </Route>

              </Switch>
          </React.Suspense>
      </HashRouter>
  )
}

export default App;